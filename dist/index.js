'use strict';

var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var ntlm = require('express-ntlm');
var fs = require('fs');
var rfs = require('rotating-file-stream');
var colors = require('colors');
var path = require('path');

require('dotenv').config({ path: 'config.env' });
require('babel-core/register');
require('babel-polyfill');

console.log('Checking for token...');
if (process.env.TFSTOKEN === "" || typeof process.env.TFSTOKEN == 'undefined') {
  console.log('Error!');
  console.log('TFSTOKEN not found in the config file'.red);
  console.log('Exiting...');
  process.exit();
} else {
  console.log('Found!'.cyan);
}

var routes = require('./routes/index');

var app = express();

if (process.env.ENVIRONMENT !== 'dev') {
  app.use(ntlm({
    domain: 'mi.corp.rockfin.com',
    domaincontroller: 'ldap://ldapquery.mi.corp.rockfin.com'
  }));
}

// logging
morgan.token('id', function (req, res) {
  if (process.env.ENVIRONMENT !== 'dev') {
    return req.ntlm.UserName;
  } else {
    return 'JSmith';
  }
});
var tokens = '[:date[clf]] - :id - :status :method :url :response-time ms';
var logDirectory = path.join(__dirname, 'logs');
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
var logStream = rfs('access.log', {
  interval: '1d',
  path: logDirectory
});
app.use(morgan(tokens));
app.use(morgan(tokens, { stream: logStream }));

// parse forms
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(expressValidator());

// serve files from react app after build
app.use(express.static(path.join(__dirname, '..', 'client/build')));

app.use('/', routes);

app.set('port', process.env.PORT || 5000);
var server = app.listen(app.get('port'), function () {
  console.log(`Server running on port ${server.address().port}`);
});

var checkTFSToken = function checkTFSToken() {};