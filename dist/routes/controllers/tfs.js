'use strict';

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var axios = require('axios');
var btoa = require('btoa');

exports.getItem = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var errors, allErrors, url, data;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            // console.log(req.params.id.split(','));
            req.checkParams('id', 'ID is not a number').isInt();
            errors = req.validationErrors();

            if (!errors) {
              _context.next = 8;
              break;
            }

            allErrors = '';

            for (error in errors) {
              allErrors += error.msg + ' ';
            }
            res.status('400');
            res.json({
              error: { errors },
              errorString: allErrors
            });
            return _context.abrupt('return');

          case 8:
            url = `http://tfs/tfs/QL/_apis/wit/workitems/${req.params.id}?$expand=all&api-version=1.0`;
            data = {};

            axios({
              method: 'get',
              url: url,
              headers: {
                'Authorization': 'Basic ' + btoa(`MI/${process.env.USERNAME}:${process.env.TFSTOKEN}`)
              }
            }).then(function (data) {
              return res.json(data.data);
            }).catch(function (error) {
              // console.log(error.response);
              if (error.response.status == '404') {
                res.json({
                  error: error.response.data,
                  errorString: 'Item ID not found'
                });
              } else {
                res.json({
                  error: error.response.data,
                  errorString: 'Unknown error'
                });
              }
            });

          case 11:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();