import React, { Component } from 'react';

class Results extends Component {
  render() {
    return (
      <div className='results'>
        {this.state.gracieVisible &&
          <Gracie 
            data={this.state.gracieData}
          />
        }
        <br/>
        {this.state.tfsVisible &&
          <Tfs
            data={this.state.tfsData}
          />
        }
      </div>
    );
  }
}

export default Results;