import React, { Component } from 'react';
import toastr from 'toastr';
import hasValue from 'has-value';
import styled from 'styled-components';
import TfsData from '../components/TfsData';

const TfsInfo = styled.div`
  padding-top: 5%;
`

const TfsCard = styled.div`
  position: relative;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-direction: column;
  flex-direction: column;
  min-width: 0;
  word-wrap: break-word;
  background-color: #fff;
  background-clip: border-box;
  border: 1px solid rgba(0,0,0,.125);
  border-radius: .25rem;
  border-left: 6px solid #0078D7;
`

class Tfs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: ''
    }
  }

  componentWillUnmount = (data) => {
    data = '';
  }

  render() {
    let data = this.props.data;
    if (data.hasOwnProperty('errorString')) {
      toastr.error(data.errorString);
    }

    return (
      <TfsInfo>
        <h5>TFS</h5>
        <TfsCard>
          {(data.hasOwnProperty('error')) &&
            <div className='card-body'>
              <h4 className='card-title text-danger'>Error</h4>
              <p className='card-text'>
                {data.errorString}
              </p>
            </div>
          }
          {(!hasValue(data)) &&
            <div className='card-body'>
              <p className='card-text'>
                Not created in TFS... yet
              </p>
            </div>
          }
          {(!data.hasOwnProperty('error') && hasValue(data)) &&
            <TfsData data={data} />
          }
        </TfsCard>
      </TfsInfo>
    );
  }
}

export default Tfs;