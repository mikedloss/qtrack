import React, { Component } from 'react';
import axios from 'axios';
import toastr from 'toastr';
import validator from 'validator';
import Search from './Search';
import Gracie from './Gracie';
import Tfs from './Tfs';

class Main extends Component {
  constructor() {
    super();
    this.state = {
      userInput: '',
      userInputChanged: false,
      selectedSearch: '',
      tfsLoading: false,
      gracieLoading: false,
      tfsVisible: false,
      gracieVisible: false,
      tfsData: {},
      gracieData: {}
    }

    this.handleInput = this.handleInput.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.getGracieData = this.getGracieData.bind(this);
    this.getTfsData = this.getTfsData.bind(this);
    this.test = this.test.bind(this);
  }

  handleInput = (e) => {
    this.setState({ 
      tfsVisible: false,
      gracieVisible: false,
      userInput: e.target.value,
      userInputChanged: true
    });
  }

  handleChange = (selected) => {
    if (selected === 'GRACIE') {
      this.setState({ selectedSearch: 'GRACIE' });
    } else if (selected === 'TFS') {
      this.setState({ selectedSearch: 'TFS' });
    } else {
      this.setState({ selectedSearch: '' })
    }
  }

  handleSearch = async (e) => {
    if (!this.state.userInput) {
      toastr.error('Nothing to search for!');
    } else if (!this.state.userInputChanged) {
      toastr.error('No change detected with the input!')
    } else if (!this.state.selectedSearch) {
      toastr.error('No system selected!');
    } else if (!validator.isInt(this.state.userInput)) {
      toastr.error('Input needs be a number!');
    } else {
      // get gracie data
      // only search again if:
      // - search box is empty (!hasValue)
      // - search box has a different value from the last time it searched
      // if (!hasValue(this.state.gracieData)) { await this.getGracieData(); }
      // else if (this.state.userInput !== this.state.gracieData.recordId.toString()) { await this.getGracieData(); }
      await this.getGracieData();
      // await this.getGracieData();
      let cont = (this.state.gracieData.getItemSuccess || 
                  this.state.gracieData.hasOwnProperty('data.tfsRequestId'))
                 ? true
                 : false;
      if (cont) {
        // get tfs data
        await this.getTfsData();
      }
      this.setState({ userInputChanged: false });
    }
  }

  getGracieData = async () => {
    // only get gracie data if it's different from what was searched before
    this.setState({ gracieVisible: false, tfsVisible: false });
    await axios(`/api/gracie/item/${this.state.userInput}`)
      .then((res) => {
        // console.log(res);
        if (res.status !== 200) {
          toastr.error(res.data.error);
        } else {
          this.setState({
            gracieData: res.data,
            gracieVisible: true
          });
        }
      })
      .catch((err) => {toastr.error(err)});
  }

  getTfsData = async () => {
    this.setState({ tfsVisible: false });
    if (this.state.gracieData.data.tfsRequestId !== "") {
      await axios(`/api/tfs/item/${this.state.gracieData.data.tfsRequestId}`)
        .then((res) => {
          this.setState({
            tfsData: res.data,
            tfsVisible: true
          })
        })
        .catch((err) => toastr.error(err));
    } else {
      this.setState({
        tfsData: {},
        tfsVisible: true
      })
    }
  }

  test = () => {
    this.handleChange('GRACIE');
    this.setState({
      userInput: '5647415',
      userInputChanged: true
    });
    this.handleSearch(' ');
  }

  render() {
    return (
      <div className='main'>
        <Search 
          input={this.handleInput}
          selected={this.handleChange}
          search={this.handleSearch}
        />
        {this.state.gracieVisible &&
          <Gracie 
            data={this.state.gracieData}
          />
        }
        {this.state.tfsVisible &&
          <Tfs
            data={this.state.tfsData}
          />
        }
        <br/>
        {/*<button type='button' className='btn btn-primary' onClick={this.test}>test (click twice)</button>*/}
      </div>
    );
  }
}

export default Main;