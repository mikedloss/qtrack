import React from 'react';
import styled from 'styled-components';
let version = require('../../package.json').version;

const FooterContent = styled.div`
  margin-top: 2%;
`

const Footer = () => {
  return (
    <FooterContent>
      {(process.env.NODE_ENV === 'development') &&
        <p className='text-muted small'>{process.env.NODE_ENV} environment <span role='img' aria-label='hammer'>🔨</span> {version}</p>
      }
      {(process.env.NODE_ENV === 'production') &&
        <p className='text-muted small'>alpha {version} - work in progress</p>
      }
    </FooterContent>
  );
};

export default Footer;