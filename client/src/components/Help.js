import React from 'react';
import githubLogo from '../media/github.png';

const Help = () => {
  return (
    <div>
      <h5>What is this?</h5>
      <p>
        QTrack is designed with one goal in mind - <strong>to see where a request is after
        approval.</strong>
      </p>
      <br/>
      <h5>Ok, so...</h5>
      <p>
        The usual flow of technology requests from the business moves from GRACIE to TFS.
        Unfortunately since there's very little integration into both systems, the requester
        only can see their request sitting at 'Approved'. Technology Project Managers help
        lessen the burden on Technology teams by helping set our TFS environment up so we can
        be efficient in our planning meetings, and setting up meetings to communicate 
        progress to the business areas that asked for things
      </p>
      <p>
        So, this tool is used to give insight into where technology requests are currently at.
      </p>
      <p>
        It is <strong>NOT</strong>:
      </p>
      <ul>
        <li>A promise for when requests will get done</li>
        <li>A tool for leverage to overly burden Business Analysts and Engineers with extra work</li>
      </ul>
      <p>
        It <strong>IS</strong>:
      </p>
      <ul>
        <li>A tool to let you know where a request currently stands</li>
        <li>A tool to let you see how much work it is for larger-requests-that-seem-small</li>
      </ul>
      <br/>
      <h5>The Future</h5>
      <p>
        This is just the first iteration of QTrack. We're looking to implement things to further
        help business areas and project managers better control priority so Technology teams
        can jump into the fray even faster.
      </p>
      <br/>
      <h5>Contact</h5>
      <p>
        Having issues? Contact <a href='mailto:mikedloss@quickenloans.com'>Mike DLoss</a> or <a href='mailto:alexdeluca@quickenloans.com'>Alex DeLuca</a>. 
        We built this idea during our Bullet Time 💡
      </p>
      <p>
        <a href='https://git/mdloss/qtrack-proto'><img src={githubLogo} alt='github link' /></a>
      </p>
    </div>
  );
};

export default Help;