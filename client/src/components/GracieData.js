import React from 'react';
import ExpandableInfo from './ExpandableInfo';
import AccordionEmpty from './AccordionEmpty';
import InfoField from './InfoField';

const GracieData = (gracie) => {
  let data = gracie.data;
  return (
    <div className="card-body">
      <h4 className="card-title">{data.title}</h4>
      <h6 className="card-subtitle mb-2">
        <span className='badge badge-primary'>{data.requestStatus}</span>
      </h6>
      {(data.gumpDescription)
        ? (<ExpandableInfo title='Gump Description' data={data.gumpDescription} />)
        : (<AccordionEmpty>No Gump Description found</AccordionEmpty>)
      }
      {(data.currentSituation)
        ? (<ExpandableInfo title='Current Situation' data={data.currentSituation} />)
        : (<AccordionEmpty>No Current Situation found</AccordionEmpty>)
      }
      {(data.proposedChange)
        ? (<ExpandableInfo title='Proposed Change' data={data.proposedChange} />)
        : (<AccordionEmpty>No Proposed Change found</AccordionEmpty>)
      }
      <br/>
      <div className='row'>
        <InfoField title='Requesters' data={data.requester} classes='col-md-6 col-12' />
        <InfoField title='Business Team' data={data.businessTeam} classes='col-md-6 col-12' />
      </div>
    </div>
  )
}


export default GracieData;