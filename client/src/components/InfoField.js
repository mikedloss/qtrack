import React from 'react';
import styled from 'styled-components';

const SmallHeader = styled.small`
  color: #868e96;
`

const InfoField = ({title, data, classes}) => {
  if (Array.isArray(data)) {
    data = data.map((each) => { return each + '; ' });
  }
  
  return (
    <div className={classes}>
      <SmallHeader>{title}</SmallHeader>
      <p>{data}</p>
    </div>
  )
}

export default InfoField;