const express = require('express');
const path = require('path');
const router = express.Router();
const gracieController = require('./controllers/gracie');
const tfsController = require('./controllers/tfs');

router.get('/api/test', (req, res) => {
    res.end(JSON.stringify(req.ntlm)); // {"DomainName":"MYDOMAIN","UserName":"MYUSER","Workstation":"MYWORKSTATION"}
});

router.get('/api/gracie/item/:id', async (req, res) => await gracieController.getItem(req, res));

router.get('/api/tfs/item/:id', async (req, res) => await tfsController.getItem(req, res));

router.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '..', '..', 'client/build/index.html'));
});

module.exports = router;