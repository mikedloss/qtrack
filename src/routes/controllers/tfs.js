const axios = require('axios');
const btoa = require('btoa');

exports.getItem = async (req, res) => {
  // console.log(req.params.id.split(','));
  req.checkParams('id', 'ID is not a number').isInt();
  const errors = req.validationErrors();
  if (errors) {
    let allErrors = '';
    for (error in errors) {
      allErrors += error.msg + ' ';
    }
    res.status('400');
    res.json({
      error: {errors},
      errorString: allErrors
    });
    return;
  }
  let url = `http://tfs/tfs/QL/_apis/wit/workitems/${req.params.id}?$expand=all&api-version=1.0`;
  let data = {};
  axios({
    method: 'get',
    url: url,
    headers: {
      'Authorization': 'Basic ' + btoa(`MI/${process.env.USERNAME}:${process.env.TFSTOKEN}`)
    }
  })
  .then((data) => res.json(data.data))
  .catch((error) => {
    // console.log(error.response);
    if (error.response.status == '404') {
      res.json({
        error: error.response.data,
        errorString: 'Item ID not found'
      });
    } else {
      res.json({
        error: error.response.data,
        errorString: 'Unknown error'
      });
    }
  });
}